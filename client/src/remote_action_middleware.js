export default socket => store => next => action => {
    console.log('in middleware action', action);
    console.log('in middleware next', next);
    console.log('in middleware store', store);
    if (action.meta && action.meta.remote)
        socket.emit('action', action);
    return next(action)
}
/*export default function(store) {
  return function(next) {
    return function(action) {

    }
  }
} */